# OTP Decoder by Chainsaw
# Decodes encrypted message using a key file (text format)
# Ver 1.2 2020-0109

import re
import sys 

if (len(sys.argv) < 2 ):
    print " ERROR -- Missing key file on command line "
    print " Correct use is ", sys.argv[0] , " key_file.txt "
    # exit program
    sys.exit()

# Key file from command line
key_file = sys.argv[1]

# Clear the memory
oDig = ""
oTxt = ""
fullkey = ""
key  = ""


# The Code for Letters to numbers. Conversion Table #1 EN
CT = { '0':"CODE", '1': 'A',  '2': 'E',  '3': 'I',  '4': 'N',  '5': 'O',  '6': 'T',  '70': 'B',  '71': 'C',  '72': 'D',  '73': 'F',  '74': 'G',  '75': 'H',  '76': 'J',  '77': 'K',  '78': 'L',  '79': 'M',  '80': 'P',  '81': 'Q',  '82': 'R',  '83': 'S',  '84': 'U',  '85': 'V',  '86': 'W',  '87': 'X',  '88': 'Y',  '89': 'Z',  '90': '#',  '91': '.',  '92': ':',  '93': "'",  '94': '( )',  '95': '+',  '96': '-',  '97': '=',  '98': '?',  '99': ' '  }

# read encrypted text from prompt
# 
# Test message with Larch key
# "46364 98742 62198 41677 85307 05675 25815 63727 90885 52687 73582 02586 98592 " 
etxt = raw_input(" Enter the encypted message: ")

#read otp key file into string
# Key file will have a header line, then blank line, followed by the key digits
# error handling for missing file
try:
    key_data = open(key_file)
except IOError:  # FileNotFoundError in Python 3
    print "ERROR-- File not found: {}".format(key_file)
    sys.exit()
with key_data as f:
    next(f) #skip header
    next(f) #skip blank line
    for line in f:
       fullkey = fullkey + line 

print "\nOTP Decrypter by Chainsaw. v1.1 \n"


#clean up the strings to numbers only
etxt = re.sub("\D", "", etxt)
fullkey  = re.sub("\D", "", fullkey)

#  Key match locator to find msg starts in mid key.
#  key_start is the first 5 numbers of the enrypted messg, 
#      which will match our key file and sets the start point
key_start = fullkey.find(etxt[:5])
if key_start == -1:
	print "Key not found. Quitting. \n"
	exit()
key = fullkey[key_start:]
#print etxt, "\n"
#print key


# add  encrypted text and key to create decrypted digits
count = len(etxt)
print "Encrypted text is ", count , " characters long."

for i in range(0, count ):
        #print  i, " ", etxt[i], " + ", key[i], " = ", 
	sum = int(etxt[i]) + int(key[i])
	if sum > 9:
	   sum = sum - 10 
	#print sum 
	oDig = oDig + str(sum)

#Format the output digits with space every 5 and return every 60(after adding spaces)
tmpTxt = ' '.join(oDig[i:i+5] for i in xrange(0,len(oDig),5)) 
tmpTxt = '\n'.join(tmpTxt[i:i+60] for i in xrange(0,len(tmpTxt),60)) 
print "Output Digits: \n", tmpTxt 
print " - - - - - - - - "

count = len(oDig)
i=5 # skip first 5 digits as they are the key id
while (i < count ):
	# get character
	if int(oDig[i]) < 7 :
	    c = oDig[i]
	    i = i+1
	else:
	    c = oDig[i] + oDig[i+1] 
	    i = i+2
	oTxt = oTxt +  CT[c]	    
# need to handle CODEs & FIGs here		
	if c == "90": # FIGure
		#read 3 at a time until next "90"
		inFig = 1
		while inFig == 1 :
			c = oDig[i] 
			oTxt = oTxt + c
			i = i + 3
			if  oDig[i] + oDig[i+1] == "90":
				oTxt = oTxt + CT["90"]
				i = i+2
				inFig = 0
	elif c == "0": # CODE
		# read only next 3 once
		c = oDig[i] + oDig[i+1] + oDig[i+2] 
		oTxt = oTxt +  c
		i = i + 3

print "Output Text: \n", oTxt
	
print "\n -- end --"


