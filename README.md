# OTP Script

Python script to decode OTP encrypted messages using a local key file.

Usage:

python otp.py  key_file.txt



Then paste the encrypted message. Here's the test message for key_file larch.txt

46364 98742 62198 41677 85307 05675 25815 63727 90885 52687 73582 02586 98592


